# Strings

Strings are merely an array of characters that are terminated by the nul character ('\0'). The array must be at least one byte longer than the string stored within. The length of the string is the number of characters excluding the string terminator ('\0').

**Why must the array be one byte longer than the string?**

> Two strings walk into a bar. The bartender says, "So what'll it be?"
> The first string says, "I think I'll have a beer quag fulk boorg jdk&CJfdLk jk3s d$\#$gasdf^u r89nvy~~owmc64^Dzx.xvcu"
> "Please excuse my friend," the second string says, "He isn't nul-terminated!"

## Declaring Strings

### Declare an Array

```c
char <variable name> [<size>];
```

### Examples:

* Char array named "instructorName" with a size of 5

```c
char instructorName [5];
```

* Char array named "inputBuffer" with a size of 256

```c
char inputBuffer [256];
```

* Char array named "studentLastName" with a size of 32

```c
char studentLastName [32];
```

**NOTE: size MUST be established during declaration**

---

## Initializing Strings

### Initialize a String

```c
char <variable name> [<size>] = {<data>, <data>, '\0'};
```

### Example 1:

```c
char instructorName [] = {'W', 'i', 'l', 'l', '\0'};
```

**NOTE: Terminating a string with the MACRO "NULL" may work, but is not recommended because it is meant for pointers.**

![](../assets/string1.png)

#### Example 2:

```c
char <variable name> [<size>] = {<data>, <data>, 0};
```

```c
char inputBuffer [256] = {0};
```

![](../assets/string2.png)

The example above declares and initializes an empty array. It also ensures the string is nul-terminated.

#### Example 3:

```c
char <variable name> [<size>] = {<data>, <data>, 0};
```

```c
char studentLastName [32] = {89, 111, 117, 0};
```

![](../assets/string3.png)

The above example declares and inits an array of size 32 that begins with the string "You".


---
**Complete Strings Labs**
