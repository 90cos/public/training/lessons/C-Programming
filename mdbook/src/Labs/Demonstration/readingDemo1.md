# Reading a File

## One char at a time
- Verify fopen() succeeded by checking for NULL pointers.
- fclose() everything you fopen().
- Read char-by-char until you hit EOF.
    - stdio.h defines a constant named EOF.
    - EOF is the end-of-file return value.
    = While reading a file stream, if you encounter EOF then you should stop reading.


```c
int main(void)
{
    FILE * myFile_ptr = fopen(“read-file.txt”, “r”); // Opens a read-only file
    char readFromFile = 0; // Store char-by-char input from myFile_ptr
    if (myFile_ptr != NULL) 	// Verify fopen() succeeded… 
    {
        while (readFromFile != EOF) // Continue reading until the end of file
        {
            readFromFile = getc(myFile_ptr); 	// Read one character
            putc(readFromFile, stdout); 		// Print that character
        }
        fclose(myFile_ptr); // Always fclose anything you fopen
    }
    else 			// …Otherwise, fopen() failed
    {
        puts(“Error opening file!”); 	// Tell the user and…
        return -1;			// …Return an error value
    }
    return 0;
}
```

## One line at a time
- Verify fopen() succeeded by checking for NULL pointers.
- fclose() everything you fopen().
- Read line-by-line until you hit EOF.
    - fgets(), among other conditions, reads until EOF.
    - At EOF, fgets() returns a NULL pointer.

```c
int main(void)
{
    FILE * myFile_ptr = fopen(“read-file.txt, “r”); // Opens a read-only file
    char tempBuff[256] = { 0 };	// Temporary buffer to store read lines
    char * tempReturnValue = tempBuff; // Holds fgets() return value
    if (myFile_ptr != NULL) 	// Verify fopen() succeeded… 
    {
        while (tempReturnValue) // Continue reading until return value is NULL
        {
            tempReturnValue = fgets(tempBuff, 256, myFile_ptr);
            if (tempReturnValue) 	// If EOF hasn’t been reached…
            {
                puts(tempBuff);	// …print the buffer
            }
        }
        fclose(myFile_ptr); // Always fclose anything you fopen
    }
    else{ return -1 } // fopen failed
    return 0;
}

```

