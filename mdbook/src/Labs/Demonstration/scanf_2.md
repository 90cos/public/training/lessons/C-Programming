# Demonstration Lab

## Formatted I/O \(scanf\)

**Formatted Input \(Numbers\)**

* Read three ints, representing the date, from one line
* Format the input so the integers are separated by a dash \(-\) as &lt;mm&gt;-&lt;dd&gt;-&lt;yyyy&gt;
* Output the results as a date with leading zeros &lt;mm&gt;/&lt;dd&gt;/&lt;yyyy&gt;
* Specify the field width of the output appropriately