# Demonstration Lab

## Basic I/O

* Input a single character then print the character
* **Syntax Example:**

```c
int userInput = 0;                // Will store user input
printf("Enter a character: ");    // Prompts user
userInput = getchar();            // Stores user input
printf("Your character was: ");   // Prefaces output
putchar(userInput);               // Prints output
```

### Discuss the output of...

* Enter a character: **7**
* Enter a character: **m**
* Enter a character: **54**
* Enter a character: **JasV**
