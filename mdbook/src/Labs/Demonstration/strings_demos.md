## Demonstration Lab 2

---

* Declare and zeroize a char array with sufficient size to store your favorite word and the nul character

```c
char myFavWord [9] = {0};
```

* Assign your favorite word to the leading elements of that array

```c
myFavWord [0] = 84;
myFavWord [1] = 115;
myFavWord [2] = 117;
myFavWord [3] = 110;
myFavWord [4] = 100;
myFavWord [5] = 101;
myFavWord [6] = 114;
myFavWord [7] = 101;
```

* Ensure your string is null-terminated by manually setting the last element to 0

```c
myFavWord [8] = 0;
```

* Print your null-terminated string

```c
printf("my favorite word is %s\n", myFavWord);
```

**NOTE: explicitly ensuring you have nul-terminated your string is a safe programming practice**

~~**EVIL:**~~ **Try replacing/removing/commenting all nul('\0') characters and print the string**

```c
char myFavWord [9] = {0}; // init a char array

myFavWord [0] = 84;  // t
myFavWord [1] = 115; // s
myFavWord [2] = 117; // u
myFavWord [3] = 110; // n
myFavWord [4] = 100; // d
myFavWord [5] = 101; // e
myFavWord [6] = 114; // r
myFavWord [7] = 101; // e

myFavWord [8] = 0; // \0

printf("My favorite word is %s!\n", myFavWord);

// Will output..
// my favorite word is tsundere!
//tsundere refers to a person that starts off cold and non receptive but then gradually warms up to their company. Often an archetype in many japanese animations.
```
