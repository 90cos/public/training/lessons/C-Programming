# Usernames

Write a C program to:
- Prompt the user for the number of students.
- Read the first, middle, and last name for each student.
- Remove any newline characters from all strings.
- Append each name to a “class roster” file in the following format:
    - For George Walker Bush, write the following to the the file... George Walker Bush has a username of gwbush.

### BUILD THOSE SAFETY CHECKS!