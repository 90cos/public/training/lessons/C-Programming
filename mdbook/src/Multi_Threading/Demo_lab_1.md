# A task with a single thread

Define a variable of the type pthread_t to store the thread identifier:
```c
pthread_t tid;

```
Create a thread and pass the identifier that was created in the preceding step to the pthread_create function. The thread is created with the default attributes. Also, specify a function that needs to be executed to create the thread:
```c
pthread_create(&tid, NULL, runThread, NULL);

```
In the function, you will be displaying a text message to indicate that the thread has been created and is running:
```c
printf("Running Thread \n");

```
Invoke a for loop to display the sequence of numbers from 1 to 5 through the running thread:
```c
for(i=1;i<=5;i++) printf("%d\n",i);

```
Invoke the pthread_join method in the main function to make the main method wait until the thread completes its task:
```c
pthread_join(tid, NULL);
```
The createthread.c program for creating a thread and making it perform a task is as follows:
```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *runThread(void *arg)
{
    int i;
    printf("Running Thread \n");
    for(i=1;i<=5;i++) printf("%d\n",i);
    return NULL;
}

int main()
{
    pthread_t tid;
    printf("In main function\n");
    pthread_create(&tid, NULL, runThread, NULL);
    pthread_join(tid, NULL);
    printf("Thread over\n");
    return 0;
}
```
## Explanation

We will define a variable called tid of the type pthread_t to store the thread identifier. A thread identifier is a unique integer, that is, assigned to a thread in the system. Before creating a thread, the message In main function is displayed on the screen. We will create a thread and pass the identifier tid to the pthread_create function. The thread is created with the default attributes, and the runThread function is set to execute to create the thread.

In the runThread function, we will display the text message Running Thread to indicate that the thread was created and is running. We will invoke a for loop to display the sequence of numbers from 1 to 5 through the running thread. By invoking the pthread_join method, we will make the main method wait until the thread completes its task. It is essential to invoke the pthread_join here; otherwise, the main method will exit without waiting for the completion of the thread.

# Output

```
$> ./createthread
In main function
Running Thread
1
2
3
4
5
Thread over
```






















