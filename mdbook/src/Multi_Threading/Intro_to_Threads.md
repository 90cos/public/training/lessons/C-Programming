C programs often perform several tasks simultaneously. For example, a program may:

* Execute procedures that accomplish intermediate tasks in parallel and so improve performance

* Process user input while carrying on time-consuming data communication or real-time operations “in the background”

Different tasks are performed simultaneously by the concurrent execution of parts of the program. Especially on modern multiprocessor systems—including multicore processors, of course—it is increasingly important for programs to take advantage of concurrency to use the system’s resources efficiently.

## Processes and threads

Whenever we run a program, the moment that it is loaded from the hard disk (or any other storage) into the memory, it becomes a process. A process is executed by a processor, and for its execution, it requires a program counter (PC) to keep track of the next instruction to be executed, the CPU registers, the signals, and so on.

A thread refers to a set of instructions within a program that can be executed independently. 

A thread pool contains a collection of threads that are waiting for tasks to be allocated to them for concurrent execution.

Basically, threads enhance the efficiency of an application through parallelism, that is, by running two or more independent sets of code simultaneously. This is called multithreading.

Multithreading is not supported by C, so to implement it, POSIX threads (Pthreads) are used. GCC allows for the implementation of a pthread.

## pthread

A thread identifier is a unique integer, that is ,assigned to a thread in the system.

The pthread_create function is invoked to create a thread. The following four arguments are passed to the pthread_create function:

* A pointer to the thread identifier, which is set by this function
* The attributes of the thread; usually, NULL is provided for this argument to use the default attributes
* The name of the function to execute for the creation of the thread
* The arguments to be passed to the thread, set to NULL if no arguments need to be passed to the thread

## Do we really need Threads?

Now, one would ask why do we need multiple threads in a process?? Why can’t a process with only one (default) main thread be used in every situation.

### Scenario

Suppose there is a process, that receiving real time inputs and corresponding to each input it has to produce a certain output. Now, if the process is not multi-threaded ie if the process does not involve multiple threads, then the whole processing in the process becomes synchronous. This means that the process takes an input processes it and produces an output.

## Differentiation between Processes and Threads

* Processes do not share their address space while threads executing under same process share the address space.
* From the above point its clear that processes execute independent of each other and the synchronization between processes is taken care by kernel only while on the other hand the thread synchronization has to be taken care by the process under which the threads are executing
* Context switching between threads is fast as compared to context switching between processes
* The interaction between two processes is achieved only through the standard inter process communication while threads executing under the same process can communicate easily as they share most of the resources like memory, text segment etc





