# FOR LOOP: OCCUPANDI TEMPORIS

In the previous questions, you should have identified your past code/programs where a for loop would have been more economical. Well NOW is your chance. Create a C file where your previous code is commented out and directly underneath, you implement a for loop.

```c
/*commented-out code from areas in previous programs that could really use a for loop*/

for (x1; x2; x3)
{
    statement;
}
```
