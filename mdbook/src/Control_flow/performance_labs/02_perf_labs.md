
# Student Performance Labs

(Printing Numbers from a Loop) Write a program that utilizes looping to print the numbers from 1 to 10 side by side
on the same line with three spaces between numbers.

 (Find the Largest Number) The process of finding the largest number (i.e., the maximum of a group of numbers)
 is used frequently in computer applications. For example, a program that determines the winner of a sales 
 contest would input the number of units sold by each salesperson. The salesperson who sells the most units 
 wins the contest. Write a pseudocode program and then a program that inputs a series of 10 non-negative numbers
 and determines and prints the largest of the numbers. Hint: Your program should use three variables as follows:

```
counter:	A counter to count to 10 (i.e., to keep track of how many numbers have
been input and to determine when all 10 numbers have been processed)

number:	The current number input to the program

largest:	The largest number found so far
```
(Validating User Input) Modify the program below to validate its inputs. On any input, 
if the value entered is other than 1 or 2, keep looping until the user enters a correct value.

```c
#include <stdio.h>

// function main begins program execution
int main( void )
{
    //Initialize variables in definitions
    unsigned int passed = 0; // number of passes
    unsigned int failures = 0; // number of failures
    unsigned int student = 1; // student counter
    int result; // one exam result

    //process 10 sudents using counter-controlled looop
    while ( student <= 10 ) {

        // prompt user for input and obtain value from user
        printf( "%s", "Enter result ( 1=pass,2=fail ): " );
        scanf( "%d", &result );

        // if result 1, increment passes
        if ( result == 1 ) {
            passes = passes + 1;
        } // end if
        else { // otherwise, increment failures
            failures = failures + 1;
        } // end else

        student = student + 1; // increment student counter
    } // end while

    // termination phase; display number of passes and failures
    printf( "Passed %u\n", passes );
    printf( "Failed %u\n", failures );

    // if more than eight students passed, print "Bonus to instructor!"
    if ( passes > 8 ) {
        puts( "Bonus to instructor!" );
    } // end if
} // end function main
```

What does the following program print?

```c
#include <stdio.h>

int main( void )
{
    unsigned int count = 1; // initialize count

    while ( count <= 10 ) { // loop 10 times

        //output line of text
        puts( count % 2 ? "****" : "++++++++");
        ++count; // increment count
    } // end while
} // end function main
```

What does the following program print?

```c
#include <stdio.h>

int main( void )
{
    unsigned int row; 10; // initialize row
    unsigned int column; // define column

    while ( row >= 1) { // loop until row < 1
        column = 1; // set column to 1 as iteration begins

        while ( column <= 10 ) { // loop 10 times
            printf( "%s", row % 2 ? "<": ">" ); // output
            ++column; // increment column
        } // end inner while

        --row // decrement row
        puts( "" ); // begin new output line
    } // end outer while
} // end function main
```

(Square of Asterisks) Write a program that reads in the side of a square and then 
prints that square out of asterisks. Your program should work for squares of all side
sizes between 1 and 20. For example, if your program reads a size of 4, it should print

```
****
****
****
****
```
(Hollow Square of Asterisks) Modify the program you wrote previously so that
it prints a hollow square. For example, if your program reads a size of 5, it should print

```
*****
*   *
*   *
*   *
*****
```

(Interest Calculator) The simple interest on a loan is calculated by the formula
```
 interest = principal * rate * days / 365;
```
The preceding formula assumes that rate is the annual interest rate, and therefore 
includes the division by 365 (days). Develop a program that will input principal, 
rate and days for several loans, and will calculate and display the simple interest for each loan, 
using the preceding formula. Here is a sample input/output dialog:


```
Enter loan principal (-1 to end): 1000.00
Enter interest rate: .1
Enter term of the loan in days: 365
The interest charge is $100.00

Enter loan principal (-1 to end): 1000.00
Enter interest rate: .08375
Enter term of the loan in days: 224
The interest charge is $51.40

Enter loan principal (-1 to end): -1

```

(Salary Calculator) Develop a program that will determine the gross pay for each of
several employees. The company pays “straight time” for the first 40 hours worked by 
each employee and pays “time-and-a-half” for all hours worked in excess of 40 hours. 
You’re given a list of the employees of the company, the number of hours each employee 
worked last week and the hourly rate of each employee. Your program should input this 
information for each employee and should determine and display the employee’s gross pay. 
Here is a sample input/output dialog:

```

Enter # of hours worked (-1 to end): 39
Enter hourly rate of the worker ($00.00): 10.00
Salary is $390.00

Enter # of hours worked (-1 to end): 40
Enter hourly rate of the worker ($00.00): 10.00
Salary is $400.00

Enter # of hours worked (-1 to end): 41
Enter hourly rate of the worker ($00.00): 10.00
Salary is $415.00

Enter # of hours worked (-1 to end): -1

```




