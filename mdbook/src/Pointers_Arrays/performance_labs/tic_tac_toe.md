# Performance Lab - Tic-Tac-Toe

* Replicate a tic tac toe grid using a two dimensional char array of global scope
* Define the following functions in accordance with the stub code, and loop through those functions taking input until:
  * someone wins
  * There are no more selections

```
int print_the_grid();
int any_plays_left();
char did_someone_win();
int what_is_your_play(char currentPlayer, int gridLocation);
```

* Use main() to loop through the functions in order, as listed, until any_plays_left() returns 0 or did_someone_win() returns something other than 0
* Specifications for these four functions are defined in the stub code
* Direct reference (see: myArray[0]) is authorized
