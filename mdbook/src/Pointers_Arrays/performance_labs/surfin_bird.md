# Performance Lab - Surfin-bird

The students must define find_the_word() as specified below.


**Do *NOT* change main()** as it contains 56 tests created for find_the_word()

```c
char * find_the_word(char * sentence_ptr, char * searchWord_ptr, int * errorCode_ptr)
```

**Return Value:** 
- char pointer to the first occurrence of searchWord in the null-terminated string found at sentence_ptr
- NULL for all other situations (e.g., sentence_ptr is NULL, searchWord_ptr is NULL, errorCode_ptr is NULL)

**Parameters**
- sentence_ptr - char pointer to a null-terminated string which represents the sentence to search
- searchWord_ptr - char pointer to a null-terminated string which represents the key to search for
- errorCode_ptr - int pointer which will store an error code provided by find_the_word (see below)
- Purpose - Safely (e.g., watch for array overruns, mind the null-terminators) locate the memory address of the string found at searchWord_ptr within sentence_ptr
  
**Requirements**
- Only Address Arithmetic is permitted to access sentence_ptr and searchWord_ptr.  
- Ignore the testing code. Only concern yourself with the function code.
- Assign your error code to the memory address found in errorCode_ptr.  
- The error codes are #defined as constant MACROS at the beginning of main() but are also listed here:
	- DEFAULT_ERROR_CODE - This is a default value used in testing. The error code variable is initialized with this value.  The error code variable is also reset to this value after each test.  
    	- You will not need to return this code.  It is entirely restricted to testing and debugging.
	- ERROR_CODE_SUCCESS - This is not an actual error.  Rather, it indicates your function has successfully completed.  Use this MACRO to indicate success.
	- ERROR_NULL_SENTENCE_POINTER - This MACRO is to be used when sentence_ptr is NULL.  
    	- The return value of the function will of course be NULL but the function also needs to store this MACRO in the memory address stored in errorCode_ptr.
	- ERROR_NULL_SEARCH_POINTER - This MACRO is to be used when searchWord_ptr is NULL. 
    	- The return value of the function will of course be NULL but the function also needs to store this MACRO in the memory address stored in errorCode_ptr.
	- ERROR_SEARCH_NOT_FOUND - This MACRO is to be used when the string located at searchWord_ptr has not been found inside the string located at sentence_ptr. 
    	- The return value of the function will of course be NULL but the function also needs to store this MACRO in the memory address stored in errorCode_ptr.