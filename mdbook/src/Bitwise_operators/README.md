---

# Bitwise Operators
---

### The slides for this topic can be found [here](https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Bitwise_operators/slides/#/)

## Topics:

* ### Bitwise Operations
* ### Why Bitwise Operations?
* ### Numbering Systems
* ### Bitwise Operators

## By the end of this lesson you should know:

* How to utilize bitwise operators
* The different numbering systems

---
